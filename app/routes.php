<?php 









$app->get('/', function () use ($app) {
  $logements = Logement::all();
  $villes = Ville::all();
  $quartiers = Quartier::all();
  $type_logements = Type_logement::all();
  $app->render('recherche.twig', array('villes' => $villes));
  $app->render('quartier.twig', array('quartiers' => $quartiers));
  $app->render('type.twig', array('type_logements' => $type_logements));
  $app->render('accueil.twig', array('logements' => $logements));
  
  //$app->render('accueil.twig', array('quartiers' => $quartiers));
});

$app->get('/logement', function() use ($app){
	$logements = Logement::all();
	$app->render('logement.php.twig', array('logements'=> $logements));

});

/*$app->get('/logement/:id', function($id) use ($app){
	if(isset($_GET['ville'])){
		echo "plop";
	}
	$logements = Logement::where('id_logement','=',$id)->get();

	$app->render('logement.php.twig', array('logements'=> $logements));

});*/

$app->get('/logement/recherche', function() use ($app){
	$sup_min  = $_GET['superficie_min'];
	$ville    = $_GET['ville'];
	$quartier = $_GET['quartier'];
	$sup_max  = $_GET['superficie_max'];
	$type     = $_GET['type'];
	$prix_min =	$_GET['prix_min'];
	$prix_max =	$_GET['prix_max'];

	$location = isset($_GET['location']) ? 1 : 0;
	$prix_reference = $location ? 'loyer' : 'prix';

	$query = new Logement;

	if (!empty($ville)) {
		$query = $query->where('id_ville', '=',
			Ville::where('nom_ville','=',$ville)->value('id_ville'));
	}
	if (!empty($quartier) && strcmp($quartier, 'vide') != 0) {
		$query = $query->where('id_quartier', '=',
			Quartier::where('nom_quartier','=',$quartier)->value('id_quartier'));
	}
	if (!empty($sup_min)) {
		$query = $query->where('superficie', '>=', $sup_min);
	}
	if (!empty($sup_max)) {
		$query = $query->where('superficie', '<=', $sup_max);
	}
	if (!empty($type)) {
		$query = $query->where('id_type', '=',
			Type_logement::where('type','=',$type)->value('id_type'));
	}
	$query = $query->where('location', '=', $location);
	if (!empty($prix_min)) {
		$query = $query->where($prix_reference, '>=', $prix_min);
	}
	if (!empty($prix_max)) {
		$query = $query->where($prix_reference, '<=', $prix_max);
	}
	$app->render('logement.php.twig', array('logements'=> $query->get()));
});

$app->get('/annonce/:id', function($id) use ($app){
	$logements = Logement::where('id_logement','=',$id)->get();
	$app->render('detail_logement.twig', array('logements'=> $logements));

});

$app->post('/ajout_annonce', function() use ($app){
	$type_logements  = Type_logement::all();
	$app->render('ajout_annonce.twig', array('type_logements' => $type_logements));
});

?>
