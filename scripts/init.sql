-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Mar 20 Octobre 2015 à 11:35
-- Version du serveur :  5.5.42
-- Version de PHP :  5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `lebonappart`
--

-- --------------------------------------------------------
--
-- Structure de la table `logements`
--

CREATE TABLE `logements` (
  `id_logement` int(11) NOT NULL UNIQUE,
  `id_user` int(11) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_ville` int(11) NOT NULL,
  `id_quartier` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `superficie` int(11) NOT NULL,
  `location` boolean NOT NULL,
  `prix` int(11),
  `loyer` int(11),
  `nb_pieces` int(11) NOT NULL,
  `description` varchar(255),
  `numero_rue` int(10) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `telephone` int(10),
  `email` varchar (255) 
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `logements` (`id_logement`, `id_user`, `password`,
 `id_ville`, `id_quartier`, `id_type`,`superficie`,`location`,
 `prix`,`loyer`,`nb_pieces`,`description`,`numero_rue`,`titre`,`telephone`,`email`) VALUES
(1,1,'toto',1,1,1,25,1,0,250,3,'Superbe appart qui sent la binouze',6,
  'Occase de fifou','0365851220','yolo@yopmail.com');


INSERT INTO `logements` (`id_logement`, `id_user`, `password`,
 `id_ville`, `id_quartier`, `id_type`,`superficie`,`location`,
 `prix`,`loyer`,`nb_pieces`,`description`,`numero_rue`,`titre`,`telephone`,`email`) VALUES
(2,5,'popo',2,2,2,100,0,60000,0,6,'Truc de fou !!! La belle vie !',6,
  'La maison de vos reves','0677963520','bobo@yopmail.com');



-- --------------------------------------------------------
--
-- Structure de la table `villes`
--
--

CREATE TABLE `villes` (
  `id_ville` int(11) NOT NULL UNIQUE,
  `nom_ville` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `villes` (`id_ville`,`nom_ville`) VALUES
(1,'nancy');
INSERT INTO `villes` (`id_ville`,`nom_ville`) VALUES
(2,'metz');
-- --------------------------------------------------------
--
-- Structure de la table `quartiers`
--
--

CREATE TABLE `quartiers` (
  `id_quartier` int(11) NOT NULL UNIQUE,
  `id_ville` int(11) NOT NULL,
  `nom_quartier` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `quartiers` (`id_quartier`,`id_ville`,`nom_quartier`) VALUES
(1,1,'stanislas');
INSERT INTO `quartiers` (`id_quartier`,`id_ville`,`nom_quartier`) VALUES
(2,2,'poincare');
-- --------------------------------------------------------
--
-- Structure de la table `type_logements`
--
--

CREATE TABLE `type_logements` (
  `id_type` int(11) NOT NULL UNIQUE,
  `type` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `type_logements` (`id_type`,`type`) VALUES
(1,'F1');
INSERT INTO `type_logements` (`id_type`,`type`) VALUES
(2,'F2');



ALTER TABLE `logements`
  ADD PRIMARY KEY (`id_logement`),
  ADD CONSTRAINT `fk_vill` FOREIGN KEY (`id_ville`) REFERENCES `villes` (`id_ville`),
  ADD CONSTRAINT `fk_quartier` FOREIGN KEY (`id_quartier`) REFERENCES `quartiers` (`id_quartier`),
  ADD CONSTRAINT `fk_type` FOREIGN KEY (`id_type`) REFERENCES `type_logements` (`id_type`);


ALTER TABLE `quartiers`
  ADD PRIMARY KEY (`id_quartier`),
  ADD CONSTRAINT `fk_ville` FOREIGN KEY (`id_ville`) REFERENCES `villes` (`id_ville`);

ALTER TABLE `villes`
  ADD PRIMARY KEY (`id_ville`);

ALTER TABLE `type_logements`
  ADD PRIMARY KEY (`id_type`);
